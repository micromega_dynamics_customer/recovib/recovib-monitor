# Communicate with the RECOVIB MONITOR
<!-- MarkdownTOC autolink="true" bracket="round" markdown_preview="markdown" -->

- [Overview](#overview)
- [Control Socket](#control-socket)
	- [Incoming Commands](#incoming-commands)
		- [GetMfgId \(ID = 4\)](#getmfgid-id-4)
		- [GetChannelCfg \(ID = 6\)](#getchannelcfg-id-6)
		- [GetInputsCfg \(ID = 34\)](#getinputscfg-id-34)
		- [GetCalibration \(ID = 8\)](#getcalibration-id-8)
		- [SetNetwork \(ID = 9\)](#setnetwork-id-9)
		- [GetNetwork \(ID = 10\)](#getnetwork-id-10)
		- [SetTLMEndPoint \(ID = 11\)](#settlmendpoint-id-11)
		- [GetTLMEndpoint \(ID = 12\)](#gettlmendpoint-id-12)
		- [GetSamplingCapabilities \(ID = 13\)](#getsamplingcapabilities-id-13)
		- [SetACQParam \(ID = 14\)](#setacqparam-id-14)
		- [GetACQParam \(ID = 15\)](#getacqparam-id-15)
		- [StartTLM \(ID = 20\)](#starttlm-id-20)
		- [RebootDevice \(ID = 21\)](#rebootdevice-id-21)
		- [GetFirmwareVersionInfo \(ID = 24\)](#getfirmwareversioninfo-id-24)
		- [SetDO \(ID = 31\)](#setdo-id-31)
		- [GetDIDO \(ID = 32\)](#getdido-id-32)
	- [Outcoming Commands](#outcoming-commands)
		- [GetMfgIdReply \(ID = 4\)](#getmfgidreply-id-4)
		- [GetChannelCfgReply \(ID = 6\)](#getchannelcfgreply-id-6)
		- [GetInputsCfgReply \(ID = 34\)](#getinputscfgreply-id-34)
		- [GetCalibrationReply \(ID = 8\)](#getcalibrationreply-id-8)
		- [GetNetworkReply \(ID = 10\)](#getnetworkreply-id-10)
		- [GetTLMEndPointReply \(ID = 12\)](#gettlmendpointreply-id-12)
		- [GetSamplingCapabilitiesReply \(ID = 13\)](#getsamplingcapabilitiesreply-id-13)
		- [GetACQParamReply \(ID = 15\)](#getacqparamreply-id-15)
		- [GetFirmwareVersionInfoReply \(ID = 24\)](#getfirmwareversioninforeply-id-24)
		- [GetDIDOReply \(ID = 32\)](#getdidoreply-id-32)
		- [CmdReply \(ID = 254\)](#cmdreply-id-254)
		- [ErrorReplyStr \(ID = 255\)](#errorreplystr-id-255)
- [Telemetry Socket](#telemetry-socket)
	- [Telemetry format](#telemetry-format)
- [Quickstart](#quickstart)
	- [Connect to MONITOR, start acquisition and start streaming](#connect-to-monitor-start-acquisition-and-start-streaming)

<!-- /MarkdownTOC -->

## Overview
The RECOVIB MONITOR basically opens 2 TCP sockets at start-up :

- A **control** socket listening on port 2000
	- This socket sends and receives messages
	- The incoming and outcoming messages are serialized using [Message Pack Format](https://msgpack.org/)
- A **telemetry** socket 
	+ This socket only sends messages
	+ The outcoming messages are **not** serialized using [Message Pack Format](https://msgpack.org/)

The control socket will be used to somehow configure the device to your needs (acquisition, telemetry enpoint, network configuration, *etc*).
The telemetry socket will be used to stream real-time data to a TCP pair.

<center><img src="Images/MONITOR_STANDALONE.svg" width="85%"></center>

## Control Socket
To connect to the device's TCP control socket, either use the device IP address or the device hostname (depending on the network config) and connect to port 2000.

Several commands are implemented to interact with the device. They respond to several rules :

- Every incoming message gets a response
- Incoming message should not exceed 1440 bytes[^1]
- Outcoming messages never exceed 1440 bytes[^1]
- Every incoming and outcoming message is serialized using [Message Pack Format](https://msgpack.org/)
- The following [Message Pack](https://msgpack.org/) general pattern is always respected :
	+ Global Array (**Array of size 2**, required) : Global array wrapping the command's components
		* Command id (**UINT8**, required) : the command numeric identifier
		* Arguments Array (**Array of size N**, required) : Array wrapping all arguments
			- Argument 1 (any [Message Pack](https://msgpack.org/) type, optional)
			- ...
			- Argument N (any [Message Pack](https://msgpack.org/) type, optional)

### Incoming Commands

#### GetMfgId (ID = 4)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 4
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetMfgIdReply](#getmfgidreply-id-4) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed


#### GetChannelCfg (ID = 6)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 6
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetChannelCfgReply](#getchannelcfgreply-id-6) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetInputsCfg (ID = 34)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 34
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetInputsCfgReply](#getinputscfgreply-id-34) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetCalibration (ID = 8)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 8
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetCalibrationReply](#getcalibrationreply-id-8) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### SetNetwork (ID = 9)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 9
	* Arguments Array (**Array of size 8**, required) : Array wrapping all arguments
		- IP Mode (**UINT8**, required) : IP Mode (Only one option so far - IPV4 : 0x00)
		- IP Gateway (**STRING**, required) : IP Gateway, formatted as IP1.IP2.IP3.IP4
		- IP Adress (**STRING**, required) : IP Adress, formatted as IP1.IP2.IP3.IP4
		- IP Net Mask (**STRING**, required) : IP Net Mask, formatted as IP1.IP2.IP3.IP4
		- IP Primary DNS (**STRING**, required) : IP Primary DNS, formatted as IP1.IP2.IP3.IP4
		- IP Secondary DNS (**STRING**, required) : IP Secondary DNS, formatted as IP1.IP2.IP3.IP4
		- Device Name (**STRING**, required) : Device Name, should not exceed 15 bytes
		- DHCP mode (**UINT8**, required) : DHCP mode (0x00 Disabled - 0x01 Enabled)

Expected Response :

- [CmdReply](#cmdreply-id-254) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetNetwork (ID = 10)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 10
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetNetworkReply](#getnetworkreply-id-10) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### SetTLMEndPoint (ID = 11)
Required Privilege Level = USER

Define the IPV4 endpoint to which the device telemetry socket will stream real-time acquisition data.
The host telemetry socket can have the same address as the host control socket but should, of course, have a different port.
Note that it might be needed to open the selected port in the firewall on the host side to allow incoming connections.

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 11
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		- Telemetry port (**UINT16**, required) : The telemetry enpoint port
		- Telemetry IPV4 address (**STRING**, required) : The telemetry enpoint IPV4 address, formatted as IP1.IP2.IP3.IP4

Expected Response :

- [CmdReply](#cmdreply-id-254) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetTLMEndpoint (ID = 12)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 12
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetTLMEndPointReply](#gettlmendpointreply-id-12) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetSamplingCapabilities (ID = 13)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 13
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetSamplingCapabilitiesReply](#getsamplingcapabilitiesreply-id-13) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### SetACQParam (ID = 14)
Required Privilege Level = USER

This command will start acquisition at a desired rate on the device. The second LED should start blinking at a rate proportionnal to the acquisition rate (blinking rate = 1/1000*acquisition rate).

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 14
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		- ADC sampling frequency (**FLOAT**, required) : The desired ADC sampling frequency (see [GetSamplingCapabilitiesReply](#getsamplingcapabilitiesreply-id-13) command)
		- Initial UTC time (**UINT32**, required) : The start value of the timestamp of the acquisition. The timestamp is increased every second and is sent along the acquisition data by the device telemetry socket (see [Telemetry Socket](#telemetry-socket)).

Expected Response :

- [CmdReply](#cmdreply-id-254) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetACQParam (ID = 15)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 15
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetACQParamReply](#getacqparamreply-id-15) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### StartTLM (ID = 20)
Required Privilege Level = USER

This command is used to start/stop streaming real-time acquisition data from the device's telemetry socket to the host endpoint previously defined by [SetTLMEndPoint](#settlmendpoint-id-11).
- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 20
	* Arguments Array (**Array of size 1**, required) : Array wrapping all arguments
		- Enable (**UINT8**, required) : 
			+ Options :
				- Stop streaming (0x00)
				- Start streaming (0x01)
				

Expected Response :

- [CmdReply](#cmdreply-id-254) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### RebootDevice (ID = 21)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 21
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [CmdReply](#cmdreply-id-254) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetFirmwareVersionInfo (ID = 24)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 24
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetFirmwareVersionInfoReply](#getfirmwareversioninforeply-id-24) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### SetDO (ID = 31)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 31
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		- Index : (**UINT8**, required) : Index of the digital output to set
		- State : (**UINT8**, required) : Desired state
			+ Options :
				* High : 0x01
				* Low : 0x00

Expected Response :

- [CmdReply](#cmdreply-id-254) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

#### GetDIDO (ID = 32)
Required Privilege Level = USER

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 32
	* Arguments Array (**Array of size 0**, required) : Array wrapping all arguments

Expected Response :

- [GetDIDOReply](#getdidoreply-id-32) if command succeeded
- [ErrorReplyStr](#errorreplystr-id-255) if command failed

### Outcoming Commands

#### GetMfgIdReply (ID = 4)
- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 4
	* Arguments Array (**Array of size 10**, required) : Array wrapping all arguments
		- Serial Number (**STRING**, required) : Serial Number, will not exceed 15 bytes
		- Model Number (**STRING**, required)  : Model Number, will not exceed 7 bytes
		- MAC Address (**STRING**, required)   : MAC Address, formatted as XX-XX-XX-XX-XX-XX
		- Base ID (**STRING**, required)       : Base ID, will not exceed 32 bytes
		- Front end ID (**STRING**, required)  : Front end ID, will not exceed 32 bytes
		- COM ID (**STRING**, required)        : COM ID, will not exceed 32 bytes
		- Supply ID (**STRING**, required)     : Supply ID, will not exceed 32 bytes
		- Backplane ID (**STRING**, required)  : Backplane ID, will not exceed 32 bytes
		- DIDO ID (**STRING**, required)       : DIDO ID, will not exceed 32 bytes
		- Model ID (**STRING**, required)      : Model ID, will not exceed 63 bytes

#### GetChannelCfgReply (ID = 6)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 6
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		- Number of channels (**UINT8**, required) : Number of acquisition channels on the device
		- Units Array (**Array of size number of channels**, required) : Array wrapping units of acquisition for all channels
			+ Units 1 (**STRING**, required) : Units, will not exceed 7 bytes
			+ ...
			+ Units *Number of channels* (**STRING**, required) : Units, will not exceed 7 bytes

#### GetInputsCfgReply (ID = 34)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 34
	* Arguments Array (**Array of size 4**, required) : Array wrapping all arguments
		- Number of connectors (**UINT8**, required) : Number of connectors on the device
		- Number of inputs (**UINT8**, required) : Number of inputs (acquisition channels + digital inputs/outputs) on the device
		- Input Id Array (**Array of size number of inputs**, required) : Array wrapping Input IDS
			+ Input ID 1 (**UINT8**, required)
			+ ...
			+ Input ID *Number of inputs* (**UINT8**, required)
		- Input Connector Id Array (**Array of size number of inputs**, required) : Array wrapping Input Connector IDS
			+ Input Connector ID 1 (**UINT8**, required) **1-based** connector id
			+ ...
			+ Input Connector ID *Number of inputs* (**UINT8**, required)

| Input ID | Description |
|:--------:|:-----------:|
|   0x01   |      CL     |
|   0x02   |     ICP     |
|    ...   |     RFU     |
|   0xFD   |      DI     |
|   0xFE   |      DO     |
|   0xFF   |    UNDEF    |


#### GetCalibrationReply (ID = 8)

Calibration parameters should be applied as follows :

```math
Y = (X[ADC]-Offset)*Gain
```

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 8
	* Arguments Array (**Array of size 3**, required) : Array wrapping all arguments
		- Offsets Array (**Array of size number of channels**, required) : Array wrapping offsets for all channels
			+ Offset 1 (**FLOAT**, required) : Offset for first channel
			+ ...
			+ Offset *Number of channels* (**FLOAT**, required) : Offset for last channel
		- Gains Array (**Array of size number of channels**, required) : Array wrapping gains for all channels
			+ Gain 1 (**FLOAT**, required) : Gain for first channel
			+ ...
			+ Gain *Number of channels* (**FLOAT**, required) : Gain for last channel
		- Calibration UTC time (**UINT32**, required) : Epoch of the calibration

#### GetNetworkReply (ID = 10)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 10
	* Arguments Array (**Array of size 8**, required) : Array wrapping all arguments
		- IP Mode (**UINT8**, required) : IP Mode (Only one option so far - IPV4 : 0x00)
		- IP Gateway (**STRING**, required) : IP Gateway, formatted as IP1.IP2.IP3.IP4
		- IP Adress (**STRING**, required) : IP Adress, formatted as IP1.IP2.IP3.IP4
		- IP Net Mask (**STRING**, required) : IP Net Mask, formatted as IP1.IP2.IP3.IP4
		- IP Primary DNS (**STRING**, required) : IP Primary DNS, formatted as IP1.IP2.IP3.IP4
		- IP Secondary DNS (**STRING**, required) : IP Secondary DNS, formatted as IP1.IP2.IP3.IP4
		- Device Name (**STRING**, required) : Device Name, should not exceed 15 bytes
		- DHCP mode (**UINT8**, required) : DHCP mode (0x00 Disabled - 0x01 Enabled)

#### GetTLMEndPointReply (ID = 12)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 12
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		- Telemetry port (**UINT16**, required) : The telemetry enpoint port
		- Telemetry IPV4 address (**STRING**, required) : The telemetry enpoint IPV4 address, formatted as IP1.IP2.IP3.IP4

#### GetSamplingCapabilitiesReply (ID = 13)

The sampling capabilities of the device is a set of acquisition frequencies that the device is able to operate at.
This floating point value has to be sent as argument of function [SetACQParam](#setacqparam-id-14).

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 13
	* Arguments Array (**Array of size 1**, required) : Array wrapping all arguments
		- Sampling Frequencies Array (**Array of size N**, required) : Array wrapping all sampling frequencies capabilities as **FLOAT**
			+ ADC sampling frequency 1 (**FLOAT**, required)
			+ ...
			+ ADC sampling frequency N (**FLOAT**, required)

#### GetACQParamReply (ID = 15)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 15
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		- ADC sampling frequency (**FLOAT**, required) : The desired ADC sampling frequency (see [GetSamplingCapabilitiesReply](#getsamplingcapabilitiesreply-id-13) command)
		- Initial UTC time (**UINT32**, required) : The start value of the timestamp of the acquisition. The timestamp is increased every second and is sent along the acquisition data by the device telemetry socket (see [Telemetry Socket](#telemetry-socket)).

#### GetFirmwareVersionInfoReply (ID = 24)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 24
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		+ Bootloader Checksum (**STRING**, required) : Checksum of the current bootloader in HEX
		+ Application version (**STRING**, required) - Version number of the current application
			+ Examples :
				+ "v2.4.0"
				+ "v99.99.99"
		+ Communication mode (**STRING**, required) : **STRING** describing the communication mode of the current app
			+ Examples : 
				+ "Cellular Network"
            	+ "WIFI Network"
            	+ "Local Area Network"
            	+ "Lora Network"
		+ Application Checksum (**STRING**, required) : Checksum of the current application in HEX
		+ Update UTC (**UINT32**, required) : Epoch time of the current application's last update
		+ Current Boot App (**UINT8**, required) : Index of the current app
		+ Firmware Update Ongoing flag (**BOOLEAN**, required) : Whether a firmware update is ongoing or not
		+ Firmware Update Ongoing Flash number of bytes (**UINT32**, required) : Number of bytes written to flash so far
		+ Firmware Update Ongoing Spifi number of bytes (**UINT32**, required) : Number of bytes written to spifi so far
		+ Firmware Update Ongoing Checksum (**UINT32**, required) : Checksum of the ongoing firmware update (given fu_n_flash and fu_n_spifi) in HEX

#### GetDIDOReply (ID = 32)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 32
	* Arguments Array (**Array of size 4**, required) : Array wrapping all arguments
		* Number of DI (**UINT8**, required) : Number of digital inputs available on the device
		* State of the DIs (**UINT16**, required) : DI i is high if bit i of this argument = 1
		* Number of DI (**UINT8**, required) : Number of digital outputs available on the device
		* State of the DOs (**UINT16**, required) : DO i is high if bit i of this argument = 1
			
#### CmdReply (ID = 254)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 254
	* Arguments Array (**Array of size 2**, required) : Array wrapping all arguments
		* Command ID replied (**UINT8**, required) : The id of the command it is replying to
		* Return Code (**INT8**, required) : A positive return code

#### ErrorReplyStr (ID = 255)

- Global Array (**Array of size 2**, required) : Global array wrapping the command's components
	* Command id (**UINT8**, required) : 255
	* Arguments Array (**Array of size 3**, required) : Array wrapping all arguments
		* Command ID replied (**UINT8**, required) : The id of the command it is replying to
		* Error Code (**INT8**, required) : A negative error code
			* Examples :
				- ERROR_MP_FORMAT : Wrong command formatting
				- ERROR_PRIVILEGE : Not sufficient rights to apply the command
				- ERROR_UNKNOWN_CMD : Wrong command ID
				- ERROR_EEPROM_SIZE : Arguments (often **STRING** arguments) is exceeding maximum specified size
				- ERROR_N_CHANNELS : Arguments missing for some channels ([SetChannelCfg](#setchannelcfg-id-5) & [SetCalibration](#setcalibration-id-7))
				- ERROR_CLOUD_RUNNING : Returned if [SetACQParam](#setacqparam-id-14) is sent when cloud client is running
				- ERROR_ALLOCATION : Allocation failed (critical)
				- ERROR_ARGS : Invalid Command Arguments
		* Error Message (**STRING**, required) : Message describing the error condition

## Telemetry Socket
The socket will stream real-time data acquired at a rate defined by the [SetACQParam](#setacqparam-id-14) control command.
The host telemetry endpoint is defined with the [SetTLMEndPoint](#settlmendpoint-id-11) control command. 
To start/stop streaming data, use the [StartTLM](#starttlm-id-20) control command.

Once the telemetry is started, the device telemetry socket connects to the defined telemetry endpoint.

### Telemetry format
Upon establishment of the connection, the device starts streaming data.
Data is sent through TCP/IP using batch of **967 bytes** formatted as follows

- [0 : 3] : TS1 - incremented once a second - start value defined by [SetACQParam](#setacqparam-id-14) command
- [4 : 5] : TS2 - incremented sampling frequency times a second - start value = 0
- [6 : 6] : Resolution in bytes
- [7 : 966] : **960 bytes** of acquisition data
	
The **960 bytes** of data are organized as follows :

| $`ACQ_{1,1}`$ | ... | $`ACQ_{N,1}`$ | ... | $`ACQ_{1,M}`$ | ... | $`ACQ_{N,M}`$ | P Reserved bytes |
|---------------|-----|---------------|-----|---------------|-----|---------------|------------------|

- where N is the number of acquisition channels
- where M is the number of acquisitions per batch for each channel. M is defined by the relation 

```math
M = \text{Floor}(\frac{960}{N*Resolution})
```
- where P is the number of reserved bytes at the end of the batch. P is defined by the relation 

```math
P = 960 - (M*N)
```

- $`ACQ_{i,j}`$ is the jth ADC output in binary twos complement format for channel i
- $`ACQ_{i,j}`$ timestamp can be recomputed based on packet TS1 and TS2. 

```math
\text{Timestamp}_{ACQ_{i,j}} = TS1 + \frac{j+TS2}{ADC_{fs}}
```

where $`ADC_{fs}`$ is the ADC sampling frequency defined by the [SetACQParam](#setacqparam-id-14) control command.

It is the responsibility of the host telemetry endpoint to convert the ADC values in electrical units (returned by the [GetChannelCfg](#getchannelcfg-id-6) command ; e.g. "mA", "V") using the calibration parameters returned by the [GetCalibration](#getcalibration-id-8) command.

```math
Y[\text{Electrical Units}] = (X[ADC]-Offset[ADC])*Gain[\text{Electrical Units}/ADC]
```

## Quickstart
### Connect to MONITOR, start acquisition and start streaming
A very simple streaming application can be implemented following these steps : 

1. Connect to the device's control socket using either its fixed IP address or its device name depending on its network configuration. Always connect to port 2000
2. Retrieve the device's usefull information :
	+ Issue the [GetChannelCfg](#getchannelcfg-id-6) command and wait for the response to retrieve the number of channels of the current device and their associated units (e.g. "mA", "V")
	+ Issue the [GetCalibration](#getcalibration-id-8) command and wait for the response to retrieve the calibration parameters that you will need to apply to the binary output data in order to get floating electrical values (see [Telemetry format](#telemetry-format))
	+ Issue the [GetSamplingCapabilities](#getsamplingcapabilities-id-13) command and wait for the response to retrieve the sampling frequency options that you can pass as argument of command [SetACQParam](#setacqparam-id-14)
3. Issue the [SetACQParam](#setacqparam-id-14) command to start the acquisition. The LED2 should start blinking
4. Create a host telemetry TCP socket and start listening on the port of your choice (make sure that your firewall settings will allow incoming connections on the selected port)
5. Issue the [SetTLMEndPoint](#settlmendpoint-id-11) command to specify where the device telemetry socket should stream its data (basically the settings decided in the previous point)
6. Issue the [StartTLM](#starttlm-id-20) command to start streaming.

Refer to the [Examples](../Examples) folder to find these steps in application.


[^1]: Maximum TCP Segment Size for this connection