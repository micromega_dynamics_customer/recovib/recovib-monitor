# RECOVIB Monitor - Quickstart Guide

## Contents
<!-- MarkdownTOC autolink="true" bracket="round" markdown_preview="markdown" -->

- [RECOVIB Monitor Network Configuration](#recovib-monitor-network-configuration)
	- [Dynamic Network Configuration](#dynamic-network-configuration)
	- [Fixed Network Configuration](#fixed-network-configuration)
		- [Device Connected to a Local Area Network \(LAN\)](#device-connected-to-a-local-area-network-lan)
		- [Device Directly Connected to a the PC](#device-directly-connected-to-a-the-pc)
- [Modify the RECOVIB Monitor Network Configuration](#modify-the-recovib-monitor-network-configuration)

<!-- /MarkdownTOC -->


## RECOVIB Monitor Network Configuration

The RECOVIB Monitor is targetted through the [RECOVIB Suite](https://gitlab.com/micromega_dynamics_customer/recovib/recovib-suite) via TCP-IP on a Local Area Network (LAN).  

The main information to know about the RECOVIB Monitor is its network configuration. You will receive the current network configuration of the device when receiving the RECOVIB Monitor.

There are basically two options: 

* The device is configured to acquire a dynamic network configuration. It acts as a [DHCP](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) client and it will be assigned a network configuration (including an IP address) by a [DHCP](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) server
* The device is configured with a fixed network configuration (including a fixed IP address)

### Dynamic Network Configuration

<center><img src="Images/SETUP_DHCP_SERVER.png" width="80%"></center>

If your device is configured with a dynamic network configuration, you need to connect it to a Local Area Network (LAN) that runs a DHCP server (the IT administrator's help might be necessary to check the availability of this service for the RECOVIB Monitor).  

Connect your PC to this **same LAN** and configure your PC network adapter accordingly.  

1. By typing "Control Panel" in the Windows Search, open the control panel
2. Open the "Network and Sharing Center"

<center><img src="Images/CONTROL_PANEL.png" width="200%"></center>

3. On the right, click "Change adapter settings"
4. Right Click the adapter of your choice ("Ethernet" in the figure below) and click "Properties"
5. Select "Internet Protocol Version 4 (TCP/IPv4)" and click "Properties"
6. Select the "Obtain an IP address automatically" option

<center><img src="Images/CONTROL_PANEL_DYNAMIC_IP.png" width="200%"></center>

Connect to the device through the [RECOVIB Suite](https://gitlab.com/micromega_dynamics_customer/recovib/recovib-suite) by using the **Device Name** option and the provided Device Name.

<center><img src="Images/DYNAMIC_IP_CONNECT.PNG" width="70%"></center>


### Fixed Network Configuration

If your device is configured with a fixed network configuration, you have two options:

#### Device Connected to a Local Area Network (LAN)

Connect your device to the LAN via the Ethernet cable.  

Make sure the fixed network configuration conforms to the LAN to which it is connected (the IT administrator's help might be necessary to check this)
- IP address
- IP gateway
- IP Network Mask
- IP Primary DNS
- IP Secondary DNS

Connect to the device through the [RECOVIB Suite](https://gitlab.com/micromega_dynamics_customer/recovib/recovib-suite) by using the **Device IP address** option and the provided IP address.

<center><img src="Images/FIXED_IP_CONNECT.PNG" width="70%"></center>


#### Device Directly Connected to a the PC

Connect your device directly to your PC via the Ethernet cable.  

You will need to configure your PC network adapter accordingly.  

E.g. if your RECOVIB Monitor is configured with the IP **192.168.0.200**, follow the below steps to be able to connect to it from your PC :

1. By typing "Control Panel" in the Windows Search, open the control panel
2. Open the "Network and Sharing Center"

<center><img src="Images/CONTROL_PANEL.png" width="200%"></center>

3. On the right, click "Change adapter settings"
4. Right Click the adapter of your choice ("Ethernet" in the figure below) and click "Properties"
5. Select "Internet Protocol Version 4 (TCP/IPv4)" and click "Properties"
6. Select the "Use the following IP address" option
7. Use an address in the range of the one configure in your RECOVIB Monitor. Here, e.g., **192.168.0.2**
8. Click in the "Subnet mask" textbox and keep the default

<center><img src="Images/CONTROL_PANEL_FIXED_IP.png" width="200%"></center>

Connect to the device through the [RECOVIB Suite](https://gitlab.com/micromega_dynamics_customer/recovib/recovib-suite) by using the **Device IP address** option and the provided IP address.

<center><img src="Images/FIXED_IP_CONNECT.PNG" width="70%"></center>



## Modify the RECOVIB Monitor Network Configuration

Note: To get access to the network configuration you would need a time-limited password for Recovib Suite. Please contact the Micromega Dynamics Support with the serial number of the Monitor.

Once connected to the RECOVIB Monitor via the options above, it is possible to modify its network configuration.  

1. Connect to the RECOVIB Monitor using one of the options above
2. Click the "Help" icon in the bottom right corner
3. Click "Manufacturing Panel"

<center><img src="Images/MANUFACTURING_PANEL.PNG" width="70%"></center>

4. You will be prompted for a password (see note above)

<center><img src="Images/PWD.PNG" width="50%"></center>

5. Get to the "Network" Tab and set the network configuration of your choice (the IT administrator's help might be necessary to set these settings)
	+ Checking the "DHCP Mode" checkbox configures the device with a [Dynamic Network Configuration](#dynamic-network-configuration)
		* Type the "Device Name" of your choice
	+ Unchecking the "DHCP Mode" checkbox configures the device with a [Fixed Network Configuration](#fixed-network-configuration). Fill the following fields with proper values.
		* "IP address"
		* "IP gateway"
		* "IP Network Mask"
		* "IP Primary DNS"
		* "IP Secondary DNS"
6. Click "Save"
7. Click "Reboot"
8. The device reboots with the new network configuration

Please remember the new network configuration set (a screenshot can help). You will need it for the next connections.

<center><img src="Images/NETWORK_CONFIG_PANEL.PNG" width="200%"></center>
<!---

<center><img src="Images/SIMPLE_PM_FSM.svg" width="70%"></center>

-->
