clear all;
close all;

%load usefull constants (Command ids, colors, ...)
usefull_constants;

%%
%MIMD parameters

%Either the hostname or the ipv4 address
device_id = 'MIMD08_0014';
%The address on which the device should send the acquisition data
%Basically set this to this computer IPV4 and use a port that is open for 
%incoming connections in the firewall settings
tlm_address = '10.0.1.133';
tlm_port = 2000;
%the number of seconds of data that will be acquired and plotted
seconds_to_acquire = 5;

%%
%1.
%Connect to the device's control socket using either 
%its fixed IP address or 
%its device name depending on its network configuration. Always connect to port 2000
t_ctl = tcpip(device_id, 2000, 'NetworkRole', 'client', 'Timeout', 0.5); 
fopen(t_ctl);
while(true)
    %2.a
    %Issue the GetChannelCfg command and wait for the response to retrieve 
    %the number of channels of the current device and their associated units (e.g. "mA", "V")
    command = dumpmsgpack({uint8(ID_GetChannelCfg), {}});
    fwrite(t_ctl, command);
    response = fread(t_ctl);
    response_parsed = parsemsgpack(response);
    response_cmd_id = response_parsed{1};
    response_arg_array = response_parsed{2};
    if(response_cmd_id == ID_GetChannelCfg)
        number_of_channels = double(response_arg_array{1});
        units = string(response_arg_array{2});
    elseif(response_cmd_id == ID_ErrorReplyStr)
        %Error replied
        fprintf('Error %i returned : (%s)\n', response_arg_array{2}, response_arg_array{3});
        break;
    else
        fprintf('%i is not an expected answer id ! \n', response_cmd_id);
        break;
    end
    %2.b
    %Issue the GetCalibration command and wait for the response to retrieve 
    %the calibration parameters that you will need to apply to the binary 
    %output data in order to get floating electrical values (see Telemetry format)
    command = dumpmsgpack({uint8(ID_GetCalibration), {}});
    fwrite(t_ctl, command);
    response = fread(t_ctl);
    response_parsed = parsemsgpack(response);
    response_cmd_id = response_parsed{1};
    response_arg_array = response_parsed{2};
    if(response_cmd_id == ID_GetCalibration)
        offsets = cell2mat(response_arg_array{1});
        gains = cell2mat(response_arg_array{2});
        calibration_utc = response_arg_array{3};
    elseif(response_cmd_id == ID_ErrorReplyStr)
        %Error replied
        fprintf('Error %i returned : (%s)\n', response_arg_array{2}, response_arg_array{3});
        break;
    else
        fprintf('%i is not an expected answer id ! \n', response_cmd_id);
        break;
    end
    %2.c
    %Issue the GetSamplingCapabilities command and wait for the response 
    %to retrieve the sampling frequency options that you can pass 
    %as argument of command SetACQParam
    command = dumpmsgpack({uint8(ID_GetSamplingCapabilities), {}});
    fwrite(t_ctl, command);
    response = fread(t_ctl);
    response_parsed = parsemsgpack(response);
    response_cmd_id = response_parsed{1};
    response_arg_array = response_parsed{2};
    if(response_cmd_id == ID_GetSamplingCapabilities)
        sampling_capabilities = cell2mat(response_arg_array{1});
    elseif(response_cmd_id == ID_ErrorReplyStr)
        %Error replied
        fprintf('Error %i returned : (%s)\n', response_arg_array{2}, response_arg_array{3});
        break;
    else
        fprintf('%i is not an expected answer id ! \n', response_cmd_id);
        break;
    end
    %3
    %Issue the SetACQParam command to start the acquisition. The LED2 should start blinking
    adc_sampling_freq = double(sampling_capabilities(1));
    %acq_start_time = posixtime(datetime('now'));
    acq_start_time = 0;
    command = dumpmsgpack({uint8(ID_SetACQParam), {single(adc_sampling_freq), uint32(acq_start_time)}});
    fwrite(t_ctl, command);
    response = fread(t_ctl);
    response_parsed = parsemsgpack(response);
    response_cmd_id = response_parsed{1};
    response_arg_array = response_parsed{2};
    if(response_cmd_id == ID_CmdReply)
        
    elseif(response_cmd_id == ID_ErrorReplyStr)
        %Error replied
        fprintf('Error %i returned : (%s)\n', response_arg_array{2}, response_arg_array{3});
        break;
    else
        fprintf('%i is not an expected answer id ! \n', response_cmd_id);
        break;
    end
    %4.
    %Create a host telemetry TCP socket and start listening on the port of your choice 
    %(make sure that your firewall settings will allow incoming connections on the selected port)
    %Accept a connection from device on the desired tlm port
    t_tlm = tcpip(device_id,tlm_port, 'NetworkRole', 'server', 'Timeout', 10);
    t_tlm.InputBufferSize = TLM_PACKET_SIZE*1000;
    %5
    %Issue the SetTLMEndPoint command to specify where the device telemetry 
    %socket should stream its data
    command = dumpmsgpack({uint8(ID_SetTLMEndPoint), {uint16(tlm_port), tlm_address}});
    fwrite(t_ctl, command);
    response = fread(t_ctl);
    response_parsed = parsemsgpack(response);
    response_cmd_id = response_parsed{1};
    response_arg_array = response_parsed{2};
    if(response_cmd_id == ID_CmdReply)
        
    elseif(response_cmd_id == ID_ErrorReplyStr)
        %Error replied
        fprintf('Error %i returned : (%s)\n', response_arg_array{2}, response_arg_array{3});
        break;
    else
        fprintf('%i is not an expected answer id ! \n', response_cmd_id);
        break;
    end
    %6
    %Issue the StartTLM command to start streaming
    command = dumpmsgpack({uint8(ID_StartTLM), {uint8(1)}});
    fwrite(t_ctl, command);
    response = fread(t_ctl);
    response_parsed = parsemsgpack(response);
    response_cmd_id = response_parsed{1};
    response_arg_array = response_parsed{2};
    if(response_cmd_id == ID_CmdReply)
        
    elseif(response_cmd_id == ID_ErrorReplyStr)
        %Error replied
        fprintf('Error %i returned : (%s)\n', response_arg_array{2}, response_arg_array{3});
        break;
    else
        fprintf('%i is not an expected answer id ! \n', response_cmd_id);
        break;
    end
    break;
end
fclose(t_ctl);
clear t_ctl;

fopen(t_tlm);

seconds_elapsed = 0;
while(true)
    batch = fread(t_tlm, TLM_PACKET_SIZE);
    header = batch(1:7);
    data = batch(8:TLM_PACKET_SIZE);
    ts1 = double(typecast(uint8(header(1:4)), 'uint32'));
    ts2 = double(typecast(uint8(header(5:6)), 'uint16'));
    resolution = double(typecast(uint8(header(7:7)), 'uint8'));
    n_measure_per_channel = floor((TLM_PACKET_SIZE-7)/(number_of_channels * resolution));
    ADC_measurements = zeros(number_of_channels, n_measure_per_channel);
    ADC_timestamps = zeros(1, n_measure_per_channel);
    for i = 1 : number_of_channels
        for j = 1 : n_measure_per_channel
            if(i == 1)
                ADC_timestamps(1,j) = ts1 + (ts2+j)/adc_sampling_freq;
            end
            index_in_data = 1+number_of_channels*resolution*(j-1)+(i-1)*resolution;
            ADC_measurements(i,j) = double(bit24toint32(data(index_in_data : index_in_data+2)));
            ADC_measurements(i,j) = (ADC_measurements(i,j)-double(offsets(i)))*double(gains(i));
            %Add any sensor conversion parameters here to convert from
            %Electrical units to physical units
        end
        subplot(number_of_channels, 1, i);
        plot(ADC_timestamps(1,:),ADC_measurements(i,:),'color', rgb_colors(i,:));
        hold on;
    end
    seconds_elapsed = seconds_elapsed + (n_measure_per_channel/adc_sampling_freq);
    if(seconds_elapsed > seconds_to_acquire)
        break;
    end
end
for i = 1 : number_of_channels
    subplot(number_of_channels, 1, i);
    if(i == number_of_channels)
        xlabel('t [s]'); 
    end
    ylabelstr = sprintf('Channel %i [%s]', i, units(i));
    ylabel(ylabelstr);
end

fclose(t_tlm);

