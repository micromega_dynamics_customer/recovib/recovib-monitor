# Interfacing RECOVIB MIMD devices through TCP/IP via MATLAB

![Results](MATLAB_example.PNG)

You should be able to run the provided [script example](mimd_matlab.m) after adapting the [following lines](mimd_matlab.m#L11) to your device and network settings.

```matlab
%Either the hostname or the ipv4 address
device_id = 'MIMD08_0014';
%The address on which the device should send the acquisition data
%Basically set this to this computer IPV4 and use a port that is open for 
%incoming connections in the firewall settings
tlm_address = '10.0.1.133';
tlm_port = 2000;
%the number of seconds of data that will be acquired and plotted
seconds_to_acquire = 5;
```

By default, the acquisition will be set to the lowest sampling frequency available on the device. 

Modify the [following lines](mimd_matlab.m#L95) to apply different settings

```matlab
adc_sampling_freq = double(sampling_capabilities(1));
```


