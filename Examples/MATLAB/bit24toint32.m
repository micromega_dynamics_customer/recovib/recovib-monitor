function ret_i32 = bit24toint32(data)
data = int32(data);
tmp = bitor(bitor(bitshift(data(1),24),bitshift(data(2),16)),bitshift(data(3),8));
ret_i32 = bitshift(tmp,-8);
end