import socket
import sys
from io import BytesIO
from msgpack import Unpacker
import binascii
from func import GetCmd, PRINT_HEX_ENABLED
from commandlist import ApiGet


#The device ip address
device_ip_address = '192.168.15.40'

tlm_port = 2000

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(5)

# Connect the socket to the port where the server is listening
con_trials = 0

try:
    while (con_trials < 3):
        server_address = (device_ip_address, tlm_port)
        print('connecting to {}:{}'.format(device_ip_address, tlm_port))
        try:
            sock.connect(server_address)
        except:
            print('Failed to connect')
            con_trials += 1
            continue
        break  # socket is connected
    else:
        raise ValueError('Errot.')
    sock.settimeout(None)
    while True:
        print("Which command do you want to send to the device?\n \
        GetMfgId\n \
        GetChannelCfg\n \
        GetCalibration\n \
        GetNetwork\n \
        GetTLMEndpoint\n \
        GetSamplingCapabilities\n \
        GetACQParam\n \
        GetComParam\n \
        GetFirmwareVersionInfo\n \
        GetDIDO\n \
        GetInputsCfg\n \
        exit\n")
        
        command = input("CMD:")
        print(command)
        if command == 'GetMfgId':
            sock.sendall(GetCmd(ApiGet.ID_GetMfgId.value).read())
        elif command == 'GetChannelCfg':
            sock.sendall(GetCmd(ApiGet.ID_GetChannelCfg.value).read())
        elif command == 'GetCalibration':
            sock.sendall(GetCmd(ApiGet.ID_GetCalibration.value).read())
        elif command == 'GetNetwork':
            sock.sendall(GetCmd(ApiGet.ID_GetNetwork.value).read())
        elif command == 'GetTLMEndpoint':
            sock.sendall(GetCmd(ApiGet.ID_GetTLMEndpoint.value).read())
        elif command == 'GetSamplingCapabilities':
            sock.sendall(GetCmd(ApiGet.ID_GetSamplingCapabilities.value).read())
        elif command == 'GetACQParam':
            sock.sendall(GetCmd(ApiGet.ID_GetACQParam.value).read())
        elif command == 'GetComParam':
            sock.sendall(GetCmd(ApiGet.ID_GetComParam.value).read())
        elif command == 'GetFirmwareVersionInfo':
            sock.sendall(GetCmd(ApiGet.ID_GetFirmwareVersionInfo.value).read())
        elif command == 'GetDIDO':
            sock.sendall(GetCmd(ApiGet.ID_GetDIDO.value).read())
        elif command == 'GetInputsCfg':
            sock.sendall(GetCmd(ApiGet.ID_GetInputsCfg.value).read())
        elif command == "exit":
            sock.close()
            break
        else:
            print("Unrecognized command, Program ended. Closing socket...")
            sock.close()
            break
        buf = BytesIO()
        unpacker = Unpacker()
        data = sock.recv(4096)
        unpacker.feed(data)
        buf.write(data)
        buf.seek(0)
        if PRINT_HEX_ENABLED:
            hex = str(binascii.hexlify(data))
            formatted_hex = ':'.join(hex[i:i + 2] for i in range(0, len(hex), 2))
            print("------------------------------------")
            print("Rx Data - Hex representation :")
            print("------------------------------------")
            print(formatted_hex)
            print("\n")
        try:
            if not data:
                print("no data")
            for unpacked in unpacker:
                print(unpacked)
        except:
            print("Error: could not read data as MsgPack.")
except ValueError:
    print('Could not connect to the device.')
finally:
    print('Program ended. Closing socket...')
    sock.close()









