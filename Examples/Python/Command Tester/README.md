# Command tester for monitor device with python

![Results](program_running.PNG)

The following example allows you to use the get commands

You should be able to run the provided [script example](main.py) after adapting the [following lines](main.py#L11) to your device and network settings.

Don't forget to install the dependencies to be able to read in MessagePack format.

```python
#The device ip address
device_ip_address = '192.168.15.40'
tlm_port = 2000
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(5)
```


By default the debugging information is enabled, you can also disable it if you want.

To do so, just change the debugging values to 1 to display or 0 to not display.


See the file: [func.py](func.py) 

```python
PRINT_MSGPCK_ENABLED=1
PRINT_HEX_ENABLED=1
```