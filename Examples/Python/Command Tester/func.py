import msgpack
from msgpack import packb, unpackb, Unpacker, Packer
from io import BytesIO
import binascii
from commandlist import ApiGet

###Debugging Information### 
PRINT_MSGPCK_ENABLED=1
PRINT_HEX_ENABLED=1
###########################


SIZOF_U8 = 0xFF

##
## @brief      Builds a "get" type message.
##
## @param      bMsg   The message
## @param      CMDID  The cmdid
##
## @return     bMsg  The bytesIO containing the message.
##
def SimpleGetMsg(bMsg, CMDID):
    "Builds a \"get\" type message."

    mypacker = Packer(use_single_float=True)
    bMsg.write(mypacker.pack_array_header(2))
    bMsg.write(mypacker.pack(CMDID))
    bMsg.write(mypacker.pack_array_header(0))
    bMsg.seek(0)
    return bMsg

##
## @brief      Unpack and print argument, in msgpack format and hex format
##
## @param      bMsg  The bytesIO containing the message
##
## @return     None
##
def unpackAndPrint(bMsg):
    "Unpack and print argument, in msgpack format and hex format"

    if PRINT_MSGPCK_ENABLED:
        print("------------------------------------")
        myunpacker = Unpacker(bMsg, use_list=1)
        print("Tx Data - MsgPack representation :")
        print("------------------------------------")
        print(myunpacker.unpack())
       

    if PRINT_HEX_ENABLED:
        bMsg.seek(0)
        hexMsg = str(binascii.hexlify(bMsg.read()))
        formatted_hex = ':'.join(hexMsg[i:i + 2] for i in range(0, len(hexMsg), 2))
        print("------------------------------------")
        print("Tx Data - Hex representation :")
        print(formatted_hex)
        print("------------------------------------")
    bMsg.seek(0)
    return

##
## @brief      Builds Cmd MsgPack.
##
## @return     bMsg  The bytesIO containing the message.
##
def GetCmd(IdCmd):

    "Builds Cmd MsgPack"
    CMDID = IdCmd
    assert isinstance(CMDID, int), 'CMDID is not an integer'
    assert (CMDID <= SIZOF_U8), 'CMDID is too large'
    assert (CMDID >= 0), 'CMDID cannot be negative'
    
    bMsg = BytesIO()
    try:
        bMsg = SimpleGetMsg(bMsg, CMDID)
        unpackAndPrint(bMsg)
        return bMsg
    except Exception as ex:
        print(ex)
        print("Could not pack Cmd.\n")
        return