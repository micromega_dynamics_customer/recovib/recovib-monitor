import socket
import sys
from io import BytesIO
from msgpack import Unpacker
import binascii
from func import GetCmd , PRINT_HEX_ENABLED, SetAcqParam, SetTlmEndpoint, StartTLM
from commandlist import ApiGet,ApiSet, UsefullConstant
from datetime import datetime, timedelta
from threading import Thread, local
import struct
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import collections 
import os
import time

version = "for test purpose only"

# Parameters
device_ip_address = '10.0.1.7'      # Monitor IP
tlm_endpoint_address = '10.0.1.46'  # Computer IP
sampling_freq = 1000                # ADC Sampling Freq (500.0, 1000.0, 2000.0, 4000.0, 8000.0, 10240.0, 16000.0, 21333.0, 32000.0, 42667.0)
seconds_to_acquire = 5              # #Number of seconds of data acquisition 
write_in_files = False
print("sampling_freq=", sampling_freq)

# debug
print_loop_stat = False

# CSV output
CSV_NB_LINE_MAX = 500000 # max=1048576

# Create a TCP/IP socket
tlm_port = 2000
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(5)

# Connect the socket to the port where the server is listening
con_trials = 0
TLM_PACKET_SIZE = 967


# file info
d = datetime.utcnow()
filedate = "{:02d}_{:02d}_{:02d}_{:02d}{:02d}{:02d}".format(d.year, d.month, d.day, d.hour, d.minute, d.second)

def bit24toint32(bIn):
    bTemp = bytearray(bIn)
    bTemp.append(0x00)
    bTemp[3] = bTemp[2]
    bTemp[2] = bTemp[1]
    bTemp[1] = bTemp[0]
    if struct.unpack('>i', bTemp)[0] >= 0:
        bTemp[0] = 0x00
    else:
        bTemp[0] = 0xff
    return (struct.unpack('>i', bTemp)[0])


def UnpackRcvData(dataRcv):
    buf = BytesIO()
    unpacker = Unpacker()
    
    unpacker.feed(dataRcv)
    buf.write(dataRcv)
    buf.seek(0)
    if PRINT_HEX_ENABLED:
        hex = str(binascii.hexlify(dataRcv))
        formatted_hex = ':'.join(hex[i:i + 2] for i in range(0, len(hex), 2))
        print("Rx Data - Hex representation :")
        print(formatted_hex)
        print("\n")
    try:
        if not dataRcv:
            print("no data")
        for unpacked in unpacker:
            print(unpacked)
    except:
        print("Error: could not read data as MsgPack.")
    return unpacked



try:
    while (con_trials < 5):
        server_address = (device_ip_address, tlm_port)
        print('connecting to {}:{}'.format(device_ip_address, tlm_port))
        try:
            sock.connect(server_address)
        except:
            print('Failed to connect')
            con_trials += 1
            continue
        break  # socket is connected
    else:
        raise ValueError('Error.')
    sock.settimeout(None)

    #Issue the GetChannelCfg command and wait for the response to retrieve 
    #the number of channels of the current device and their associated units (e.g. "mA", "V")
    sock.sendall(GetCmd(ApiGet.ID_GetChannelCfg.value).read())
    #Receive Data from command
    data = sock.recv(4096)
    #Put data in channelCfgData
    channelCfgData = UnpackRcvData(data)
    if channelCfgData[0] == ApiGet.ID_GetChannelCfg.value :
        #Retrieve the number of channels
        numberOfChannels = channelCfgData[1][0]
        #Get units
        channelsUnits = channelCfgData[1][1]
    elif channelCfgData[0] == UsefullConstant.ID_ErrorReplyStr.value:
        print('Error {} returned : ({})'.format(channelCfgData[1][1], channelCfgData[1][2]))
        sock.close()
        sys.exit()
    else:
        print('The socket received an unexpected response')
        sock.close()
        sys.exit()


    
    #Issue the GetCalibration command and wait for the response to retrieve 
    #the calibration parameters that you will need to apply to the binary 
    #output data in order to get floating electrical values (see Telemetry format)

    sock.sendall(GetCmd(ApiGet.ID_GetCalibration.value).read())
    #Receive Data from command
    data = sock.recv(4096)
    #Put data in Calibration
    calibrationData=UnpackRcvData(data)

    if calibrationData[0] == ApiGet.ID_GetCalibration.value :
        calibrationOffsets = calibrationData[1][0] #Offsets
        calibrationGains = calibrationData[1][1]    #Gain
    elif calibrationData[0] == UsefullConstant.ID_ErrorReplyStr.value:
        print('Error {} returned : ({})'.format(calibrationData[1][1], calibrationData[1][2]))
        sock.close()
        sys.exit()
    else:
        print('The socket received an unexpected response')
        sock.close()
        sys.exit()


    #Issue the GetSamplingCapabilities command and wait for the response 
    #to retrieve the sampling frequency options that you can pass 
    #as argument of command SetACQParam

    sock.sendall(GetCmd(ApiGet.ID_GetSamplingCapabilities.value).read())
    #Receive Data from command
    data = sock.recv(4096)

    #Put data in samplingCapabilitiesData
    samplingCapabilitiesData = UnpackRcvData(data)
    
    if samplingCapabilitiesData[0] == ApiGet.ID_GetSamplingCapabilities.value :
        samplingCapabilities = samplingCapabilitiesData[1][0]
    elif samplingCapabilitiesData[0] == UsefullConstant.ID_ErrorReplyStr.value:
        print('Error {} returned : ({})'.format(samplingCapabilitiesData[1][1], samplingCapabilitiesData[1][2]))
        sock.close()
        sys.exit()
    else:
        print('The socket received an unexpected response')
        sock.close()
        sys.exit()



    #Issue the SetACQParam command to start the acquisition. The LED2 should start blinking
    if sampling_freq in samplingCapabilities:
        adcSamplingFreq = float(sampling_freq)
        idx = samplingCapabilities.index(sampling_freq)
        print("sampling freq found: {} Hz, idx {}", adcSamplingFreq, idx)
    else:
        print('Sampling frequency ({}) is not available'.format(samp_freq))
        print('Available sampling freq = ', samplingCapabilities)
        sys.exit()
        
    acqStartTime = 0
    sock.sendall(SetAcqParam(adcSamplingFreq,acqStartTime).read())
    #Receive Data from command
    data = sock.recv(4096)
    Resp = UnpackRcvData(data)

    if Resp[0] == UsefullConstant.ID_CmdReply.value:
        print('SetAcqParam OK')
    elif Resp[0] == UsefullConstant.ID_ErrorReplyStr.value:
        print('Error {} returned : ({})'.format(Resp[1][1], Resp[1][2]))
        sock.close()
        sys.exit()
    else:
        print('The socket received an unexpected response')
        sock.close()
        sys.exit()

    #Issue the SetTLMEndPoint command to specify where the device telemetry 
    #socket should stream its data

    sock.sendall(SetTlmEndpoint(tlm_port,tlm_endpoint_address).read())
    #Receive Data from command
    data = sock.recv(4096)
    Resp = UnpackRcvData(data)

    if Resp[0] == UsefullConstant.ID_CmdReply.value:
        print('SetTlmEndpoint OK')
    elif Resp[0] == UsefullConstant.ID_ErrorReplyStr.value:
        print('Error {} returned : ({})'.format(Resp[1][1], Resp[1][2]))
        sock.close()
        sys.exit()
    else:
        print('The socket received an unexpected response')
        sock.close()
        sys.exit()

    #Issue the StartTLM command to start streaming
    sock.sendall(StartTLM(1).read())
    #Receive Data from command
    data = sock.recv(4096)
    Resp = UnpackRcvData(data)

    if Resp[0] == UsefullConstant.ID_CmdReply.value:
        print('StartTLM OK')
    elif Resp[0] == UsefullConstant.ID_ErrorReplyStr.value:
        print('Error {} returned : ({})'.format(Resp[1][1], Resp[1][2]))
        sock.close()
        sys.exit()
    else:
        print('The socket received an unexpected response')
        sock.close()
        sys.exit()


except ValueError:
    print('Could not connect to the device.')
finally:
    print('closing socket...')
    sock.close()


# Function for handling connections.
def serverthread(conn,numberOfChannels,adcSamplingFreq,calibrationOffsets,calibrationGains,seconds_to_acquire, channelsUnits):
    # init arrays
    array_size_max = int(seconds_to_acquire * adcSamplingFreq)
    list_timestamp = list()
    array_idx = 0

    #array_test = []
    array_data = [list() for i in range(numberOfChannels)] 

    # loop stat
    loop_cnt = 0
    loop_timer = 0
    seconds_elapsed = 0
    last_seconds_elapsed = 0
    loop_dur_max = 0
    # error stat
    error_threshold_in_s = 80 * 1.1 / adcSamplingFreq
    error_cnt = 0
    last_timestamp = 0.0

    print('\n*Data acquisition started @ {} Hz (duration = {} s ({:.2f} minutes))*'.format(adcSamplingFreq, seconds_to_acquire, seconds_to_acquire/60))

    while True:

        # Receiving from client
        try:
            data = conn.recv(TLM_PACKET_SIZE*1)
            if not data:
                print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'no data')
                break

            else:
                #decode timestamp
                ts1 = data[0:4] #Take 4 bytes
                ts1Unpack= struct.unpack('<I', ts1)[0] 
                ts2 = data[4:6] # Take 2 bytes
                ts2Unpack = struct.unpack('<H', ts2)[0]
                resolution = data[6] #Take resolution
                acqData = data[7:967] #Take data 
                
                n_measure_per_channel = math.floor((TLM_PACKET_SIZE-7)/(numberOfChannels * resolution))
                
                if array_idx + n_measure_per_channel  > array_size_max :
                    print('\n*Data acquisition completed*')
                    break

				# temporary
                timestamp = ts1Unpack + (ts2Unpack)/adcSamplingFreq
                if (last_timestamp == 0):
                    last_timestamp = timestamp
                delta = last_timestamp-timestamp
                if (delta > error_threshold_in_s):
                    #print("\r\nlast={}, curr={}, delta={}", last_timestamp, timestamp, delta)
                    error_cnt += 1
                    print("\r\nerror_cnt=", error_cnt)
                last_timestamp = timestamp

                dbg_start = time.time()
                for j in range(n_measure_per_channel):
                    list_timestamp.append(ts1Unpack + (ts2Unpack+j)/adcSamplingFreq)

                    for i in range(numberOfChannels):
                        index_in_data = numberOfChannels*resolution*(j)+(i)*resolution
                        array_data[i].append(acqData[index_in_data:index_in_data+3])

                    array_idx += 1
            
                print('*', end="", flush=True)

                # loop stat
                if print_loop_stat:
                    seconds_elapsed = seconds_elapsed + (n_measure_per_channel/adcSamplingFreq)
                    loop_cnt += 1
                    loop_timer += (time.time()-dbg_start)

                    if last_seconds_elapsed != int(seconds_elapsed):
                        last_seconds_elapsed = int(seconds_elapsed)
                        loop_dur = loop_timer / loop_cnt
                        if loop_dur > loop_dur_max:
                            loop_dur_max = loop_dur
                        print("\nloop avg time = {:.3f} ms ({} pkt per sec)".format(1000 * loop_dur, loop_cnt))
                        loop_timer = 0
                        loop_cnt = 0
            
        except socket.error:
                print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'socket blocked')
            

        if not data:
            print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'no data')
            break
    
    conn.close()
    if print_loop_stat:
        print("loop max time = {:.3f} ms".format(loop_dur_max * 1000))

    print("Data acquisition ended with {} errors".format(error_cnt))

    array_size = len(list_timestamp)
    #print("array_size=", array_size)

    # Convert data
    array_data_converted = np.zeros((numberOfChannels, array_size))
    for i in range(numberOfChannels):
        array_data_converted[i] = (np.array(list(map(bit24toint32, array_data[i])))-float(calibrationOffsets[i]))*float(calibrationGains[i])

    # write into files
    if write_in_files:
        dbg_start = time.time()
        file_idx = 0
        line_cnt = 0
        filename = "data_{}_{}.csv".format(filedate, file_idx)
        f = open(filename, 'w', encoding='utf-8')
        print("{} created".format(filename))
        for j in range(array_size):

            if line_cnt > CSV_NB_LINE_MAX: 
                # open new file if necessary
                file_idx += 1
                f.close()
                filename = "data_{}_{}.csv".format(filedate, file_idx)
                f = open(filename, 'w', encoding='utf-8')
                print("{} created".format(filename))
                line_cnt = 0

            f.write("{};".format(list_timestamp[j]))
            
            for i in range(numberOfChannels):
                if i < (numberOfChannels -1):
                    f.write("{};".format(array_data_converted[i,j]))
                else:
                    f.write("{}".format(array_data_converted[i,j]))

            f.write("\n")
            line_cnt += 1

    # init dataDict
    dataDict = collections.defaultdict(list)
    # for j in range(array_size):
    dataDict["Timestamp"] = list_timestamp
    for i in range(numberOfChannels):
        # dataDict["Channel_"+str(i+1)].append(array_data_converted[i,j])
        dataDict["Channel_"+str(i+1)] = list(array_data_converted[i])


    while True:
        print('which channel do you want to display? max:{}. (Enter exit if you want to close the program)'.format(numberOfChannels))

        command = input("CMD:")
        if command == 'exit':
            break
        elif int(command) > 0 and int(command) <= int(numberOfChannels):
            print("OK")

            fig = plt.figure()
            fig.suptitle("Channel_"+str(command), fontsize=15)
            plt.xlabel('Seconds', fontsize=15)
            plt.ylabel('Values '+channelsUnits[int(command)-1], fontsize=15)
            plt.plot(dataDict["Timestamp"], dataDict["Channel_"+str(command)])
            plt.show()
        else:
            print("Bad channel, please select a correct channel")
    

###############################################################
# Main script
###############################################################

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'Socket created')

# Bind socket to local host and port
try:
    s.bind((tlm_endpoint_address, tlm_port))
except socket.error :
    print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'Bind failed.')
    sys.exit()
print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'Socket bind complete')
# Start listening on socket
s.listen(10)
print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'Socket now listening')

# now keep talking with the client
while True:
    # wait to accept a connection - blocking call
    conn, addr = s.accept()
    print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'New connection: {}:{}'.format(addr[0], addr[1]))

    # start new thread
    thread = Thread(target=serverthread, args=(conn,numberOfChannels,adcSamplingFreq,calibrationOffsets,calibrationGains,seconds_to_acquire,channelsUnits,))
    thread.start()
    thread.join()
    print(datetime.now().strftime('[%y-%m-%d %H:%M:%S] ') + 'thread terminated.')
    break

s.close()