import enum




class ApiGet(enum.Enum):
    ID_GetMfgId = 4
    ID_GetChannelCfg = 6
    ID_GetCalibration = 8
    ID_GetNetwork = 10
    ID_GetTLMEndpoint = 12
    ID_GetSamplingCapabilities = 13
    ID_GetACQParam = 15
    ID_GetComParam = 19
    ID_GetFirmwareVersionInfo = 24
    ID_GetDIDO = 32
    ID_GetInputsCfg  = 34

class ApiSet(enum.Enum):
    ID_SetACQParam = 14
    ID_SetTLMEndPoint = 11
    ID_StartTLM = 20


class UsefullConstant(enum.Enum):
    ID_ErrorReplyStr = 255
    ID_CmdReply = 254
