import msgpack
from msgpack import packb, unpackb, Unpacker, Packer
from io import BytesIO
import binascii
from commandlist import ApiGet, ApiSet

PRINT_MSGPCK_ENABLED=1
PRINT_HEX_ENABLED=1
SIZOF_U8 = 0xFF
SIZOF_U32 = 0xFFFFFFFF


##
## @brief      Builds a "get" type message.
##
## @param      bMsg   The message
## @param      CMDID  The cmdid
##
## @return     bMsg  The bytesIO containing the message.
##

def SimpleGetMsg(bMsg, CMDID):
    "Builds a \"get\" type message."

    mypacker = Packer(use_single_float=True)
    bMsg.write(mypacker.pack_array_header(2))
    bMsg.write(mypacker.pack(CMDID))
    bMsg.write(mypacker.pack_array_header(0))
    bMsg.seek(0)
    return bMsg

##
## @brief      Unpack and print argument, in msgpack format and hex format
##
## @param      bMsg  The bytesIO containing the message
##
## @return     None
##
def unpackAndPrint(bMsg):
    "Unpack and print argument, in msgpack format and hex format"

    if PRINT_MSGPCK_ENABLED:
        myunpacker = Unpacker(bMsg, use_list=1)
        print("Tx Data - MsgPack representation :")
        print(myunpacker.unpack())

    if PRINT_HEX_ENABLED:
        bMsg.seek(0)
        hexMsg = str(binascii.hexlify(bMsg.read()))
        formatted_hex = ':'.join(hexMsg[i:i + 2] for i in range(0, len(hexMsg), 2))
        print("Tx Data - Hex representation :")
        print(formatted_hex)
    print("------------------------------")
    bMsg.seek(0)
    return

##
## @brief      Builds GetChannelCfg MsgPack.
##
## @return     bMsg  The bytesIO containing the message.
##
def GetCmd(IdCmd):

    "Builds GetCmd MsgPack"
    CMDID = IdCmd
    assert isinstance(CMDID, int), 'CMDID is not an integer'
    assert (CMDID <= SIZOF_U8), 'CMDID is too large'
    assert (CMDID >= 0), 'CMDID cannot be negative'
    
    bMsg = BytesIO()
    try:
        bMsg = SimpleGetMsg(bMsg, CMDID)
        unpackAndPrint(bMsg)
        return bMsg
    except Exception as ex:
        print(ex)
        print("Could not pack GetCmd.\n")
        return

##
## @brief      Builds SetAcqParam MsgPack.
##
## @param      adcSamplingFreq and acqStartTime
##
## @return     bMsg  The bytesIO containing the message.
##
def SetAcqParam(adcSamplingFreq,acqStartTime):
    "Builds Set AcqParam MsgPack"
    

    CMDID = ApiSet.ID_SetACQParam.value

    assert isinstance(CMDID, int), 'CMDID is not an integer'
    assert (CMDID <= SIZOF_U8), 'CMDID is too large'
    assert (CMDID >= 0), 'CMDID cannot be negative'

    assert isinstance(adcSamplingFreq, float), 'adcSamplingFreq is not a float'

    assert isinstance(acqStartTime, int), 'acqStartTime is not an interger'
    assert (acqStartTime <= SIZOF_U32), 'acqStartTime is too large'
    assert (acqStartTime >= 0), 'acqStartTime cannot be negative'


    bMsg = BytesIO()
    mypacker = Packer( use_single_float=True)
    try:
        bMsg.write(mypacker.pack_array_header(2))
        bMsg.write(mypacker.pack(CMDID))
        bMsg.write(mypacker.pack_array_header(2))
        bMsg.write(mypacker.pack(adcSamplingFreq))
        bMsg.write(mypacker.pack(acqStartTime))

        bMsg.seek(0)
        unpackAndPrint(bMsg)
        return bMsg
    except:
        print('Could not pack SetAcqParam.')
        return


##
## @brief      Builds SetTlmEndpoint MsgPack.
##
## @param      tlm_port and tlm_endpoint_address
##
## @return     bMsg  The bytesIO containing the message.
##
def SetTlmEndpoint(tlm_port,tlm_endpoint_address):
    "Builds Set Telemetry Endpoint MsgPack"
    
    CMDID = ApiSet.ID_SetTLMEndPoint.value

    assert isinstance(CMDID, int), 'CMDID is not an integer'
    assert (CMDID <= SIZOF_U8), 'CMDID is too large'
    assert (CMDID >= 0), 'CMDID cannot be negative'

    assert isinstance(tlm_port, int), 'tlm_port is not an integer'
    assert (CMDID <= SIZOF_U8), 'tlm_port is too large'
    assert (CMDID >= 0), 'tlm_port cannot be negative'

    assert isinstance(tlm_endpoint_address, str), 'tlm_address is not a string'

    
    bMsg = BytesIO()
    mypacker = Packer( use_single_float=True)
    try:
        bMsg.write(mypacker.pack_array_header(2))
        bMsg.write(mypacker.pack(CMDID))
        bMsg.write(mypacker.pack_array_header(2))
        bMsg.write(mypacker.pack(tlm_port))
        bMsg.write(mypacker.pack(tlm_endpoint_address))

        bMsg.seek(0)
        unpackAndPrint(bMsg)
        return bMsg
    except:
        print('Could not pack SetTlmEndpoint.')
        return


##
## @brief      Builds StartTLM MsgPack.
##
## @param      start 
##
## @return     bMsg  The bytesIO containing the message.
##

def StartTLM(start):
    "Builds Set StartTLM MsgPack"
    
    CMDID = ApiSet.ID_StartTLM.value

    assert isinstance(CMDID, int), 'CMDID is not an integer'
    assert (CMDID <= SIZOF_U8), 'CMDID is too large'
    assert (CMDID >= 0), 'CMDID cannot be negative'

    assert isinstance(start, int), 'start is not an integer'
    assert (CMDID <= SIZOF_U8), 'start is too large'
    assert (CMDID >= 0), 'start cannot be negative'
    

    bMsg = BytesIO()
    mypacker = Packer( use_single_float=True)
    try:
        bMsg.write(mypacker.pack_array_header(2))
        bMsg.write(mypacker.pack(CMDID))
        bMsg.write(mypacker.pack_array_header(1))
        bMsg.write(mypacker.pack(start))

        bMsg.seek(0)
        unpackAndPrint(bMsg)
        return bMsg
    except:
        print('Could not pack StartTLM.')
        return