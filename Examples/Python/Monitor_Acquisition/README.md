# Example of data acquisition with python



The script runs under python 3

You should be able to run the provided [script example](main.py).

Don't forget to install the necessary dependencies such as MessagePack etc. 


Please configure the following variables:


```python
#The device ip address
device_ip_address = '192.168.15.40'
tlm_endpoint_address = '192.168.15.39'

```

**device_ip_address** is the ip address of the monitor.

**tlm_endpoint_address** is the ip address of the computer.

So replace the IP addresses with your own. 

You can also configure 
- the duration of the acquisition :

```python
seconds_to_acquire = 5            # #Number of seconds of data acquisition 

```
- the sampling frequency
```python
#Number of seconds of data acquisition 
sampling_freq = 1000               # ADC Sampling Freq (500.0, 1000.0, 2000.0, 4000.0, 8000.0, 10240.0, 16000.0, 21333.0, 32000.0, 42667.0)

```
- the option "write_in_files": if True, write data in csv files at the end of the acquisition.


By default the debugging information is enabled, you can also disable it if you want.

To do so, just change the debugging values to 1 to display or 0 to not display.


See the file: [func.py](func.py) 

```python
PRINT_MSGPCK_ENABLED=1
PRINT_HEX_ENABLED=1
```


Once the parameters have been correctly configured, run the script.

You will then be able to choose the channel to display :

![Results](program_running.PNG)

![Results](program_running-2.PNG)



